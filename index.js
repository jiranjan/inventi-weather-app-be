const express = require('express');
const path = require('path');
const cors = require('cors');
const dotenv = require('dotenv');

const logger = require('./middleware/logger');

const app = express();

dotenv.config();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/v1/weather', require('./routes/api/v1/weather'))

// app.use(logger);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
