const express = require('express');
const router = express.Router();
const axios = require('axios');

const members = require('./../../../members');

router.get('/', (req, res) => {
  res.json(members);
});

router.get('/:city', (req, res) => {
  let data = [];
  axios.get(`${process.env.WEATHER_API}/weather?q=${req.params.city}&APPID=${process.env.APP_ID}&units=metric`)
  .then(response => {
    data.push(response.data)
    axios.get(`https://restcountries.eu/rest/v2/capital/${req.params.city}`)
    .then(response => {
      const resData = response.data;
      data.push(resData);
      res.json(data);
    })
    .catch(err => res.status(err.response.status).json(err))
  })
  .catch(err => res.status(err.response.status).json(err))
});

router.get('/forecast/:city', (req, res) => {
  axios.get(`${process.env.WEATHER_API}/forecast?q=${req.params.city}&APPID=${process.env.APP_ID}&units=metric`)
  .then(response => {
    const { data } = response;
    res.json(data);
  })
  .catch(err => res.status(err.response.status).json(err))
  // // const found = members.some(m => m.id === Number(req.params.id));
  // //
  // // if (found) res.json(members.find(m => m.id === Number(req.params.id)));
  // // else res.status(404).json({ message: 'No member with this id exists' });
  // axios.get(`api.openweathermap.org/data/2.5/weather?q=${req.params.city}&APPID=d0f9e212d4efb45116472e886d64219e`)
  // .then(weatherData => res.status(200).json(weatherData))
  // .catch(err => res.json(err));
});

router.post('/', (req, res) => {
  res.send(req.body);
});

module.exports = router;
